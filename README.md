# Présentation : Numérique Responsable en 2024 (On éteint le wifi?)

https://libreemploi.frama.io/presentation-numerique-responsable/

Cette présentation est en accès libre via internet, elle est une fresque, un état des lieux du numérique en 2024, autour des axes écologiques, économiques et sociétaux. Elle remet en cause le modèle de "développement durable" appliqué au numérique et présente les perspectives de l'industrie du numérique pour les années à venir.

La fin de la présentation ouvre le débat aux différents possibles, autant individuels que collectifs, à travers des actions possibles, et des démarches créatives afin de faire durer nos appareils, tout en se libérant des chaines d'une industrie toujours plus privatrice et oppressante pour les citoyens.

## Usage

- Ouvrir la page web
- F11 pour passer en plein écran
- Echap pour passer en mode navigation dans les slides
- S pour activer le mode présentateur (multi-écrans, ouvre une popup qui peut être bloquée par votre navigateur)
- . pour passer en mode pause (écran noir)
- les liens sont en vert

## Contribuer ? Partager ?

La présentation est en parage libre sous licence creative commons. Sur le mode présentateur j'ai ajouté des notes. Vous pouvez bien entendu contribuer à ce projet en y proposant des merge requests via git. Le fichier HTML est dans /public/index.html